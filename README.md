# touch_switch_arduino

Code to switch the lighting of a led when a cable is touched <br/>

## Materiel
  * Arduino nano
  * 220 ohm resistor
  * 1M ohm resistor
  * LED
  * Cables

## Make one
  * wire the diagram
  * upload code